# `hahn-ApplicationProcess-December2020-Frontend`

This project is the frontend side of Hanh Project.

This project is bootstrapped by [aurelia-cli](https://github.com/aurelia/cli).

For more information, go to https://aurelia.io/docs/cli/webpack

## Setup

Please ensure Git client and NodeJS (version 10 or above) are installed on your machine.

You can download NodeJS here: https://nodejs.org/en/

You can download Git client here: https://git-scm.com/downloads

Run `npm install` inside the solution folder to install all necessaries packages.

Edit api.ts inside src/config folder to check/modify `backendUrl` endpoint.

Be sure Hahn Backend program is running.

## Run dev app

Run `npm start`, then open `http://localhost:8080`

You can change the standard webpack configurations from CLI easily with something like this: `npm start -- --open --port 8888`. However, it is better to change the respective npm scripts or `webpack.config.js` with these options, as per your need.


## Build for production

Run `npm run build`, or the old way `au build --env prod`.

