export default {
  backendUrl: 'http://localhost:5000/api/',
  countriesUrl: 'https://restcountries.eu/rest/v2/',
}
