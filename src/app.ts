import { PLATFORM } from 'aurelia-framework';
import { Router, RouterConfiguration } from 'aurelia-router';

export class App {
  router: Router | undefined;

  configureRouter(config: RouterConfiguration, router: Router) {
    config.map([{
      route: [ '', 'applicants' ],
      name: 'ApplicantList',
      moduleId: PLATFORM.moduleName('./applicants/list'),
    }, {
      route: 'applicants/:id',
      name: 'ApplicantUpdate',
      moduleId: PLATFORM.moduleName('./applicants/applicant-form'),
    }, {
      route: 'applicants/create',
      name: 'ApplicantCreate',
      moduleId: PLATFORM.moduleName('./applicants/applicant-form'),
    }, {
      route: 'applicants/success',
      name: 'ApplicantSuccess',
      moduleId: PLATFORM.moduleName('./applicants/success'),
    }]);

    this.router = router;
  }
}

