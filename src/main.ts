import { Aurelia } from 'aurelia-framework';
import * as environment from '../config/environment.json';
import { PLATFORM } from 'aurelia-pal';
import { TCustomAttribute } from 'aurelia-i18n';
import { I18N } from 'aurelia-i18n';
import { ValidationMessageProvider } from 'aurelia-validation';
import { Expression } from 'aurelia-binding';
import Backend from 'i18next-xhr-backend';
import LanguageDetector from 'i18next-browser-languagedetector';

export function configure(aurelia: Aurelia): void {
  aurelia.use
    .standardConfiguration()
    .plugin(PLATFORM.moduleName('aurelia-validation'))
    .plugin(PLATFORM.moduleName('aurelia-i18n'), (instance: any) => {
      instance.i18next.use(MyLanguageDetector);

      let aliases = ['t', 'i18n'];

      // add aliases for 't' attribute
      TCustomAttribute.configureAliases(aliases);

      // register backend plugin
      instance.i18next.use(Backend);

      // adapt options to your needs (see http://i18next.com/docs/options/)
      // make sure to return the promise of the setup method, in order to guarantee proper loading
      return instance.setup({
        backend: {
          loadPath: './localization/{{lng}}.json',
        },
        attributes: aliases,
        fallbackLng: 'en',
        debug: false
      });
    })
    .plugin(PLATFORM.moduleName('aurelia-dialog'), (config) => {
      config.useDefaults();
      config.settings.lock = true;
      config.settings.centerHorizontalOnly = false;
      config.settings.startingZIndex = 5;
      config.settings.keyboard = true;
    })
    .feature(PLATFORM.moduleName('applicants/validation/index'))
    .feature(PLATFORM.moduleName('resources/index'));

  // Configure validation translations
  ValidationMessageProvider.prototype.getMessage = function(key): Expression {
    const i18n = aurelia.container.get(I18N);
    const translationId = `validations.${key}`;
    let translation = i18n.tr(translationId);
    if (translation === translationId) {
      translation = i18n.tr(key);
    }

    return (this as any).parser.parse(translation);
  };

  ValidationMessageProvider.prototype.getDisplayName = (propertyName: string, displayName: string): string => {
    const i18n = aurelia.container.get(I18N);
    return i18n.tr(displayName);
  };

  // LanguageDetector.oldDetect = LanguageDetector.prototype.detect;
  // LanguageDetector.prototype.detect = (detectionOrder? : string[]): string => {
  //   console.log(this.oldDetect);
  //   return this.oldDetect(detectionOrder);
  // }

  aurelia.use.developmentLogging(environment.debug ? 'debug' : 'warn');

  if (environment.testing) {
    aurelia.use.plugin(PLATFORM.moduleName('aurelia-testing'));
  }

  aurelia.start().then(() => aurelia.setRoot(PLATFORM.moduleName('app')));
}

class MyLanguageDetector extends LanguageDetector {
  detect(detectionOrder? : string[]) {
    return super.detect(detectionOrder).split('-')[0];
  }
}
