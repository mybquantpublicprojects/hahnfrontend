import { ApplicantEditModel } from '../applicants/applicant-edit-model';
import { HttpClient } from 'aurelia-http-client';
import { autoinject } from 'aurelia-framework';
import api from '../config/api';

@autoinject
export class ApplicantsService {
  constructor(private httpClient: HttpClient) {
  }

  getAll() {
    return new Promise((resolve, reject) => {
      this.httpClient
        .get(`${api.backendUrl}applicants`)
        .then(response => {
          resolve(response.content);
        })
        .catch(error => {
          reject(error);
        });
    });
  }

  get(id: number) {
    return new Promise((resolve, reject) => {
      this.httpClient
        .get(`${api.backendUrl}applicants/${id}`)
        .then(response => {
          resolve(response.content);
        })
        .catch(error => {
          reject(error);
        });
    });
  }

  create(applicant: ApplicantEditModel) {
    return new Promise((resolve, reject) => {
      this.httpClient
        .post(`${api.backendUrl}applicants`, applicant)
        .then(response => {
          resolve(response);
        })
        .catch(error => {
          reject(error);
        });
    });
  }

  update(id: number, applicant: ApplicantEditModel) {
    return new Promise((resolve, reject) => {
      this.httpClient
        .put(`${api.backendUrl}applicants/${id}`, applicant)
        .then(response => {
          resolve(response);
        })
        .catch(error => {
          reject(error);
        });
    });
  }

  delete(id: number) {
    return new Promise((resolve, reject) => {
      this.httpClient
        .delete(`${api.backendUrl}applicants/${id}`)
        .then(response => {
          resolve(response);
        })
        .catch(error => {
          reject(error);
        });
    });
  }

  validate(name: string, value: string): Promise<boolean> {
    return new Promise<boolean>((resolve, reject) => {
      if (name) {
        this.httpClient
          .get(`${api.backendUrl}applicants/validate/${name}`, { value })
          .then(response => {
            resolve(response.isSuccess);
          })
          .catch(error => {
            resolve(false);
          });
      } else {
        resolve(false);
      }
    });
  }
}
