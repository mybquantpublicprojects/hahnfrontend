import { I18N } from 'aurelia-i18n';

export class LocalizationService {
  i18n: I18N;
  locales: any;
  currentLocale: string;

  static inject = [I18N];
  constructor(i18n: I18N) {
    this.i18n = i18n;

    this.locales = [
      {
        title: 'English',
        code: 'en'
      },
      {
        title: 'Italiano',
        code: 'it'
      }
    ]
  }

  setLocale(code: string) {
    this.i18n.setLocale(code);
  }

  tr(value: string) {
    return this.i18n.tr(value);
  }
}
