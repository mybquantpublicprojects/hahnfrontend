import { HttpClient } from 'aurelia-http-client';
import { autoinject } from 'aurelia-framework';
import api from '../config/api';

@autoinject
export class CountriesService {

  countries: Map<string,any>;

  constructor(private httpClient: HttpClient) {
  }

  getAll():Promise<Map<string,any>> {
    return new Promise<Map<string,any>>((resolve, reject) => {
      if (!this.countries) { 
        this.httpClient
          .get(api.countriesUrl)
          .then(response => {         
            this.countries = new Map<string, any>();            
            let data = response.content;
            for (let i of data){
              //console.log(i);
              i.translations.en = i.name;
              this.countries.set(i.alpha3Code.toLowerCase(), i.translations);
              //this.countries[i.alpha3Code] = i.translations;
            }
            resolve(this.countries);
          })
          .catch(error => {
            reject(error);
          });
      } else {
        resolve(this.countries);
      }
    });
  } 
}
