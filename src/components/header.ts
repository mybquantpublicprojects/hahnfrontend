import { LocalizationService } from '../services/localization-service';
import { inject } from 'aurelia-framework';

@inject(LocalizationService)
export class Header {
  localization: LocalizationService;

  constructor(localization: LocalizationService) {
    this.localization = localization;
  }

  setLocale(code: string) {
    this.localization.setLocale(code);
  }
}
