import { autoinject, customAttribute } from 'aurelia-framework';
import { CountriesService } from '../services/countries-service'
import { I18N, I18N_EA_SIGNAL } from 'aurelia-i18n';
import { EventAggregator } from 'aurelia-event-aggregator';


@autoinject
@customAttribute('country')
export class CountryAttribute {
  value: any;
  locale: string;
  countries: Map<string,any>;

  constructor(private element: Element, private countriesService: CountriesService, private i18n: I18N, private ea: EventAggregator) {
  }

  bind() {
    this.locale = this.i18n.getLocale();
    
    this.countriesService.getAll().then(countries => {
      this.countries = countries;
      this.element.innerHTML = this.countries.get(this.value)[this.locale];
    });

    this.ea.subscribe(I18N_EA_SIGNAL, (param: any) => {
      this.locale = param.newValue;
      this.element.innerHTML = this.countries.get(this.value)[this.locale];
    });
  }
}
