import { inject } from 'aurelia-framework';
import { DialogController } from 'aurelia-dialog';

@inject(DialogController)
export class AlertDialog {
  title: string;
  message: string;

  constructor(private controller: DialogController) {
  }

  activate(model: any) {
    this.title = model.title;
    this.message = model.message;
  }
}
