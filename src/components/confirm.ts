import { inject } from 'aurelia-framework';
import { DialogController } from 'aurelia-dialog';

@inject(DialogController)
export class ConfirmDialog {
  message: string;

  constructor(private controller: DialogController) {
  }

  activate(message: string) {
    this.message = message;
  }
}
