import { ApplicantsService } from '../services/applicants-service'
import { inject } from 'aurelia-framework'
import { Applicant } from '../interfaces/applicant'
import { LocalizationService } from '../services/localization-service'
import { DialogService } from 'aurelia-dialog'
import { ConfirmDialog } from '../components/confirm'

@inject(ApplicantsService, LocalizationService, DialogService)
export class ApplicantList {
  applicantsList: Applicant[];
  dialogService: DialogService;

  constructor(private applicantsService: ApplicantsService, localization: LocalizationService, dialogService: DialogService) {
    this.dialogService = dialogService;
  }

  activate() {
    this.load();
  }

  load() {
    this.applicantsService.getAll()
      .then(applicants => {
        this.applicantsList = applicants as Applicant[];
      }).catch(error => {
        console.log(error);
    });
  }

  delete(id: number) {
    this.dialogService.open({ viewModel: ConfirmDialog, model: 'deleteApplicant' }).whenClosed(option => {
      if (!option.wasCancelled) {
        this.applicantsService.delete(id)
          .then(() => {
            this.load();
          }).catch(error => {
            console.log(error);
          });
      }
    });
  }
}
