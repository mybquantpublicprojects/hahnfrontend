import { ApplicantsService } from '../../services/applicants-service';
import { FrameworkConfiguration } from 'aurelia-framework';
import { ValidationRules } from 'aurelia-validation';

export function configure(config: FrameworkConfiguration) {
  const applicantService = config.container.get(ApplicantsService);

  ValidationRules.customRule(
    'remoteCountry', 
    value => applicantService.validate('country', value),
    ''
  );

  ValidationRules.customRule(
    'remoteEmail',
    value => applicantService.validate('email', value),
    ''
  );
}
