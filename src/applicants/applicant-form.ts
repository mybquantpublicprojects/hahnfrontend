import { inject } from 'aurelia-framework'
import { Applicant } from '../interfaces/applicant'
import { ValidationControllerFactory, ValidationController, Validator, ValidationRules, validateTrigger } from 'aurelia-validation'
import { BootstrapFormRenderer } from '../components/bootstrap-form-renderer'
import { Router } from 'aurelia-router'
import { ApplicantsService } from '../services/applicants-service'
import { CountriesService } from '../services/countries-service'
import { DialogService } from 'aurelia-dialog'
import { ConfirmDialog } from '../components/confirm'
import { AlertDialog } from '../components/alert'
import { ApplicantEditModel } from 'applicants/applicant-edit-model'
import { areEqual } from '../utils';
import { I18N, I18N_EA_SIGNAL } from 'aurelia-i18n';
import { EventAggregator } from 'aurelia-event-aggregator';

@inject(ValidationControllerFactory, Validator, DialogService, ApplicantsService, Router, CountriesService, I18N, EventAggregator)
export class ApplicantForm {
  controller: ValidationController;
  canSave: boolean;
  canReset: boolean;

  action: string;

  locale: string;

  countries: Map<string,any>;

  applicantId: number;
  applicant: ApplicantEditModel;
  originalApplicant: ApplicantEditModel;

  constructor(controllerFactory: ValidationControllerFactory, private validator: Validator, private dialogService: DialogService, private applicantsService: ApplicantsService, private router: Router, private countriesService: CountriesService, private i18n: I18N, private ea: EventAggregator) {
    this.controller = controllerFactory.createForCurrentScope();
    this.controller.validateTrigger = validateTrigger.blur;
    this.controller.addRenderer(new BootstrapFormRenderer());
    this.controller.subscribe(event => this.validate(event));

    this.originalApplicant = new ApplicantEditModel();

    this.countriesService.getAll().then(countries => {
      this.countries = countries;
    });

    this.ea.subscribe(I18N_EA_SIGNAL, (param: any) => {
      this.locale = param.newValue;
    });
    this.locale = this.i18n.getLocale();

    this.canSave = false;
    this.canReset = false;
  }

  activate(params: any) {
    if (params.id) {
      this.action = 'update';
      this.applicantsService.get(params.id)
        .then((response: Applicant) => {
          this.applicantId = response.id;

          this.applicant = new ApplicantEditModel(response);
          Object.assign(this.originalApplicant, this.applicant);

          this.controller.addObject(this.applicant);

          this.controller.validate();
        })
        .catch(error => {
          if (error.statusCode == 404) {
            this.router.navigateToRoute('ApplicantList');
          } else {
            this.dialogService.open({ viewModel: AlertDialog, model: error.statusText });
          }
        });
    } else {
      this.action = 'create';

      this.applicant = new ApplicantEditModel();
      Object.assign(this.originalApplicant, this.applicant);

      this.controller.addObject(this.applicant);
    }
  }

  validate(event: any) {
    this.canReset = !areEqual(this.originalApplicant, this.applicant);

    if (this.action == 'update') {
      this.canSave = (this.canReset && !this.controller.errors.length);
    } else {
      if (this.canReset) {
        this.validator.validateObject(this.applicant)
          .then(results => {
            this.canSave = results.every(result => {
              return result.valid;
            });
          });
      }
    }
  }

  submit() {
    this.controller.validate().then(() => {
      if (!this.controller.errors.length) {
        if (this.action == 'update') {
          this.applicantsService.update(this.applicantId, this.applicant)
            .then(response => {
              this.router.navigateToRoute('ApplicantSuccess');
            })
            .catch(error => {
              // console.log(error);
              const response = JSON.parse(error.response);
              this.dialogService.open({ viewModel: AlertDialog, model: { title: response.title, message: response.detail } });
            });
        } else {
          this.applicantsService.create(this.applicant)
            .then(response => {
              this.router.navigateToRoute('ApplicantSuccess');
            })
            .catch(error => {
              // console.log(error);
              const response = JSON.parse(error.response);
              this.dialogService.open({ viewModel: AlertDialog, model: { title: response.title, message: response.detail } });
            });
        }
      }
    });
  }

  reset() {
    this.dialogService.open({ viewModel: ConfirmDialog, model: 'resetForm' }).whenClosed(option => {
      if (!option.wasCancelled) {
        setTimeout(() => {
          Object.assign(this.applicant, this.originalApplicant);

          this.controller.reset();
          this.canSave = false;
          this.canReset = false;

          if (this.action == 'update') {
            this.controller.validate();
          }
        }, 0);
      }
    });
  }
}
