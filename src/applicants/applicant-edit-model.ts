import { Applicant } from '../interfaces/applicant';
import { ValidationRules } from 'aurelia-validation'

export class ApplicantEditModel {
  name: string = '';
  familyName: string = '';
  address: string = '';
  countryOfOrigin: string = '';
  eMailAddress: string = '';
  age: string = '';
  hired: boolean = false;

  constructor(applicant?: Applicant) {
    if (applicant != null) {
      this.name = applicant.name;
      this.familyName = applicant.familyName;
      this.address = applicant.address;
      this.countryOfOrigin = applicant.countryOfOrigin;
      this.eMailAddress = applicant.eMailAddress;
      this.age = applicant.age.toString();
      this.hired = applicant.hired;
    }
  }
}

ValidationRules
.ensure('name')
.displayName('applicant.fields.name')
.required()
.minLength(5)
.ensure('familyName')
.displayName('applicant.fields.familyName')
.required()
.minLength(5)
.ensure('address')
.displayName('applicant.fields.address')
.required()
.minLength(10)
.ensure('countryOfOrigin')
.displayName('applicant.fields.countryOfOrigin')
.required()
// .then()
// .satisfiesRule('remoteCountry')
// .withMessageKey('country')
.ensure('eMailAddress')
.displayName('applicant.fields.eMailAddress')
.required()
.then()
.satisfiesRule('remoteEmail')
.withMessageKey('email')
.ensure('age')
.displayName('applicant.fields.age')
.required()
.then()
.range(20, 60)
.on(ApplicantEditModel);

